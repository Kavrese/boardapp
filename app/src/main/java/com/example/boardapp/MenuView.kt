package com.example.boardapp

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.menu_layout.*

fun initMenu(context: MainActivity): MenuView{
    val menuView = MenuView(context)
    menuView.init()
    return menuView
}

class MenuView(private val context: MainActivity) {
    private val list_view_item = arrayListOf<ImageView>(
        context.item1,
        context.item2,
        context.item3,
        context.item4,
        context.item5,
        context.item6,
    )

    fun init(){
        for (i in list_view_item){
            i.setOnClickListener {
                setAllItemNoActiv()
                setColorItemMenu(i, R.color.activ_item)
            }
            setColorItemMenu(i, R.color.no_activ_item)
        }
        setColorItemMenu(context.item1, R.color.activ_item)
    }

    private fun setAllItemNoActiv(){
        for (i in list_view_item){
            setColorItemMenu(i, R.color.no_activ_item)
        }
    }

    private fun setColorItemMenu(view: ImageView, colorId: Int){
        view.setColorFilter(ContextCompat.getColor(context.applicationContext, colorId))
    }
}